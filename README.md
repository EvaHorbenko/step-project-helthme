Step Project "Cards"

Учасники: 
    Бельдюгин Евгений; 
    Горбенко Ева

Фрагменты написанные учасниками: 

    Бельдюгин Евгений:

        дизайн + верстка
            header
            отботражение карточек
            полныя информация на карточках
        создание и подключение  gulpfile.js

        wait-function.js
        message.js + их верстка
        preloader.js + его верстка

        visit.js
        visit-therapist.js
        visit-dentist.js
        visit-cardiologist.js
        visits-collection.js

        методы: getToken в server.js (совместно)
        методы: loginError, hideAndShow в login-modal.js 
        методы: getAccess, changeAccess в login-modal.js (совместно)


    Горбенко Ева:

        доработка дизайна + верстка
            фильтры
            модальные окна + работа с инпутами
        доработка gulpfile.js 

        modal.js
        login-modal.js
        card-modal.js
        full-visit-info-modal.js
        server.js

        методы: takeChangedValues в visit-therapist.js (совместно)
        методы: takeChangedValues, addDiseases в visit-cardiologist.js
        методы: getVisits, removeVisit, saveChanges, checkVisitAsDone, getChanges, filter, search, getObjectValues в visits-collection.js

