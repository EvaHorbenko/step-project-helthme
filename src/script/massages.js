const showMassages = async () => {
    const root = document.querySelector('.root')
    const card = document.querySelector('.card')
    const massagesContainer = document.querySelector('.massage-container')

    if (!root.contains(card)) {
        massagesContainer.classList.remove('hidden')
    } else if (root.contains(card)) {
        massagesContainer.classList.add('.hidden')
    }
    const massages = document.querySelectorAll('.js-massage')
    await wait(1000)
    massages[0].classList.add('massage--active')

    await wait(1000)
    massages[1].classList.add('massage--active')
}

const hideMassages = () => {
    const massagesWrapper = document.querySelector('.massage-container')
    massagesWrapper.style.display = 'none'
}