class LoginModal extends Modal {
    constructor () {
        super()

        this.inputPassword
        this.passwordIcon
        this.closeModalIcon
        this.autoriseBtn
        this.loginModalWrapper
    }

    renderModalWrapper() {
        super.renderModalWrapper()
        this.modalWrapper.innerHTML = `
            <div class="modal modal-login">
                <div class="modal-header">
                    <h2 class="modal-title">авторизация</h2>
                    <button class="close-btn js-close-login-modal-btn"> <i class="close-icon fas fa-times"></i> </button>
                </div>
                <div class="input-wrapper">
                    <input type="text" class="modal-input login js-login-input" placeholder="Введите свой e-mail">
                    <input type="password" class="modal-input password js-password-input" placeholder="Введите свой пароль">
                    <i class="toggle-password-icon fas fa-eye-slash"></i> 
                </div>
                <button class="button autorise-btn js-autorise-button">авторизироваться</button>
            </div>
        `
        this.loginModalWrapper = document.querySelector('.modal-login')
        this.modalWrapper.addEventListener('click', (event) => {
            if(event.target.classList.contains('modal-wrapper')) {
                this.removeModal()
            }
        })

        this.passwordIcon = document.querySelector('.toggle-password-icon')
        this.passwordIcon.addEventListener('click', this.togglePassword.bind(loginModal))
        this.closeModalIcon = document.querySelector('.js-close-login-modal-btn')
        this.closeModalIcon.addEventListener('click', this.removeModal.bind(loginModal))
        this.autoriseBtn = document.querySelector('.js-autorise-button')
        this.autoriseBtn.addEventListener('click', server.getToken.bind(server))
        
        this.inputPassword = document.querySelector('.modal-input.password')
    }

    togglePassword () {
        this.inputPassword.type === 'password' ? this.inputPassword.type = 'text' : this.inputPassword.type = 'password'

        this.passwordIcon.classList.toggle('far')
        this.passwordIcon.classList.toggle('fa-eye')
        this.passwordIcon.classList.toggle('fas')
        this.passwordIcon.classList.toggle('fa-eye-slash')
    }

    async checkIsLogged() {
        try {
            await server.getToken()
        } catch (error) {
            console.log(error.message);
        }
    }

    loginError() {
        const errorText = document.createElement('span')
        errorText.classList.add('modal-error')
        errorText.innerHTML = 'вы ввели неверный e-mail или пароль'

        this.loginModalWrapper.prepend(errorText)
    }

    changeAccess() {
        document.querySelector('.js-full-filter-wrapper').classList.toggle('hidden-full-filter-wrapper')
        this.hideAndShow('.js-login-toggle-button')
        this.hideAndShow('.js-card-toggle-button')
        this.hideAndShow('.js-logout-button')
    }

    hideAndShow(selector) {
        const element = document.querySelector(selector)
        element.classList.toggle('hidden')
    }

    logout() {
        localStorage.removeItem('token')
        this.changeAccess()
        document.querySelectorAll('.card').forEach(card => card.remove())
        visitsCollection.visitsList = []
        hideMassages()
    }
}

let loginModal = new LoginModal('login')
loginModal.checkIsLogged()

const openLoginModalBtn = document.querySelector('.login-button')
openLoginModalBtn.addEventListener('click', loginModal.renderModalWrapper.bind(loginModal))

const logoutBtn = document.querySelector('.js-logout-button')
logoutBtn.addEventListener('click', loginModal.logout.bind(loginModal))