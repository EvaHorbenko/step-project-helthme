class VisitCardiologist extends Visit {
    addInfoModal() {
        super.addInfoModal()
        const div = this.fullInfoModal.querySelector('.js-info')
        
        div.innerHTML = `
                        <h4 class="full-card__info-title">Возраст:</h4>
                        <span class="full-card__info-text">${this.unicValues.age}</span><br/>
                        <h4 class="full-card__info-title">Давление:</h4>
                        <span class="full-card__info-text">${this.unicValues.pressure}</span><br/>
                        <h4 class="full-card__info-title">Индекс массы тела:</h4>
                        <span class="full-card__info-text">${this.unicValues.bodyMassIndex}</span><br/>
                        <ul class="full-card__info-title">Заболевания сердечно-сосудистой системы:</ul>
        `
        
        const ul = div.querySelector('ul')
        this.unicValues.diseases.forEach(diseas => {
            const li = document.createElement('li')
            li.classList.add('full-card__list-item')
            li.innerHTML = diseas
            ul.appendChild(li)
        })

        const editBtn = this.fullInfoModal.querySelector('.js-edit-btn')
        editBtn.addEventListener('click', this.editVisit.bind(this))
    }

    editVisit() {
        super.editVisit()
        const unicInfoContainer = this.fullInfoModal.querySelector('.js-info')
        unicInfoContainer.innerHTML = `
                                        <input value="${this.unicValues.pressure}" class="modal-input pressure js-pressure-edit-input edit-input" placeholder="обычное давление">
                                        <input value="${this.unicValues.bodyMassIndex}" class="modal-input body-mass-index js-body-mass-index-edit-input edit-input" placeholder="индекс массы тела">
                                        <input value="${this.unicValues.age}" class="modal-input age js-age-edit-input edit-input" placeholder="возраст">
                                        <div class="diseases-collection">
                                            <button class="add-diseases-btn"> <i class="add-diseases-icon fas fa-plus"></i> </button>
                                        </div>
                                        <div class="full-card__save-btn-wrapper">
                                            <button class="full-card__button js-save-btn full-card__button-save js-full-visit-info-toggle-button">Сохранить</button>
                                        </div>
        `

        this.unicValues.diseases.forEach(disease => {
            this.addDiseases(disease)
        })

        const addDiseasesBtn = unicInfoContainer.querySelector('.add-diseases-btn')
        addDiseasesBtn.addEventListener('click', () => this.addDiseases(''))

        document.querySelector('.diseases-collection').addEventListener('click', event => cardModal.removeDiseases(event))

        const saveChangesBtn = document.querySelector('.js-save-btn')
        saveChangesBtn.addEventListener('click', this.takeChangedValues.bind(this))
        saveChangesBtn.addEventListener('click', fullVisitInfoModal.removeModal.bind(fullVisitInfoModal))
    }

    addDiseases(disease) {
        const diseasesCollection = document.querySelector('.diseases-collection')

        const inputDiseaseWrapper = document.createElement('span')
        inputDiseaseWrapper.classList.add('input-disease__wrapper')
        inputDiseaseWrapper.innerHTML = `
            <input value="${disease}" class="js-diseases-edit-input modal-input diseases" placeholder="перенесенные заболевания CCC">
            <i class="remove-diseases-icon fas fa-backspace"></i>
        `
        diseasesCollection.appendChild(inputDiseaseWrapper)
    }
}