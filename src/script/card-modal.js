class CardModal extends Modal {
    constructor () {
        super()

        this.patientInputs
        this.addVisitBtn
        this.doctorSelector
        this.addDiseasesBtn
    }

    renderModalWrapper() {
        super.renderModalWrapper()
        this.modalWrapper.innerHTML = `
            <div class="modal modal-card">
                <div class="modal-header">
                    <h2 class="modal-title">новый визит</h2>
                    <button class="close-btn js-close-card-modal-btn js-card-toggle-button"> <i class="close-icon fas fa-times"></i> </button>
                </div>
                <div class="input-wrapper card-input">
                    <div class="doctor">
                        <div class="select-wrapper">
                            <select class="modal-input select js-doctor-select" name="select">
                                <option class="option" value="choose" selected disabled>Виберите врача</option>
                                <option class="option" value="therapist">Терапевт</option>
                                <option class="option" value="dentist">Стоматолог</option>
                                <option class="option" value="cardiologist">Кардиолог</option>
                            </select>
                        </div>
                        <input type="text" class="modal-input purpose js-purpose-input" placeholder="Укажите цель визита">
                        <textarea style="resize: none;" class="modal-input description js-description-input" placeholder="Краткое описание визита" rows="3"></textarea>
                        <div class="select-wrapper">
                            <select class="modal-input select js-urgency-input" name="select">
                                <option class="option" value="choose" selected disabled>Виберите срочность</option>
                                <option class="option" value="regular">обычная</option>
                                <option class="option" value="priority">приоритетная</option>
                                <option class="option" value="urgent">неотложная</option>
                            </select>
                        </div>
                    </div>
                    <div class="patient">
                        <div class="full-name">
                            <input type="text" class="modal-input last-name js-last-name-input" placeholder="Фамилия">
                            <input type="text" class="modal-input first-name js-first-name-input" placeholder="Имя">
                            <input type="text" class="modal-input father-name js-father-name-input" placeholder="Отчество">
                        </div>
                        <div class="js-modal-variable-inner variable-inputs"></div>
                    </div>
                </div>
                <button class="button add-visit-btn js-card-toggle-button">добавить визит</button>
            </div>
        `

        this.closeModalIcon = document.querySelector('.js-close-card-modal-btn')
        this.closeModalIcon.addEventListener('click', this.removeModal.bind(cardModal))
        this.addVisitBtn = document.querySelector('.add-visit-btn')
        this.addVisitBtn.addEventListener('click', visitsCollection.addVisit.bind(visitsCollection))
        this.addVisitBtn.addEventListener('click', this.removeModal.bind(cardModal))
        this.doctorSelector = document.querySelector('.js-doctor-select')
        this.doctorSelector.addEventListener('change', this.choseDoctor.bind(cardModal))

        this.modalWrapper.addEventListener('click', (event) => {
            if(event.target.classList.contains('modal-wrapper')) {
                this.removeModal()
            }
        })

        this.patientInputs = document.querySelector('.variable-inputs')
    }

    choseDoctor() {
        if (this.doctorSelector.value === 'cardiologist') {
            this.patientInputs.innerHTML = `<input class="modal-input pressure" placeholder="обычное давление">
                                            <input class="modal-input body-mass-index" placeholder="индекс массы тела">
                                            <input class="modal-input age" placeholder="возраст">
                                            <div class="diseases-collection">
                                                <span class="input-disease__wrapper">
                                                    <input class="modal-input diseases" placeholder="перенесенные заболевания CCC">
                                                    <i class="remove-diseases-icon fas fa-backspace"></i>
                                                </span>
                                                <button class="add-diseases-btn"> <i class="add-diseases-icon fas fa-plus"></i> </button>
                                            </div>`
            this.addDiseasesBtn = document.querySelector('.add-diseases-btn')
            this.addDiseasesBtn.addEventListener('click', this.addDiseases.bind(this))
            document.querySelector('.diseases-collection').addEventListener('click', event => this.removeDiseases(event))
        } else if (this.doctorSelector.value === 'dentist') {
            this.patientInputs.innerHTML = ` <input class="modal-input last-visit" placeholder="дата последнего посещения">`
        } else if (this.doctorSelector.value === 'therapist') {
            this.patientInputs.innerHTML = ` <input class="modal-input age" placeholder="возраст">`
        }
    }

    addDiseases () {
        const diseasesCollection = document.querySelector('.diseases-collection')
        const newDisease = document.createElement('span')
        newDisease.classList.add('input-disease__wrapper')
        newDisease.innerHTML = `
            <input class="modal-input diseases" placeholder="перенесенные заболевания CCC">
            <i class="remove-diseases-icon fas fa-backspace"></i>`
        diseasesCollection.appendChild(newDisease)
        this.patientInputs.scrollTo({
            top: 1000,
            behavior: 'smooth'
        })
    }

    removeDiseases (event) {
        let target = event.target
        if (target.classList.contains('remove-diseases-icon') && document.querySelectorAll('.input-disease__wrapper').length > 1) {
            target.closest('.input-disease__wrapper').remove()
        }
    }
}

let cardModal = new CardModal()

const openVisitModalBtn = document.querySelector('.js-card-button')
openVisitModalBtn.addEventListener('click', cardModal.renderModalWrapper.bind(cardModal))
openVisitModalBtn.addEventListener('click', hideMassages)
