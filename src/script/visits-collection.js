class VisitsCollection {
    constructor() {
        this.visitsList = []
    }
    
    async getVisits() {
        const visits = await server.getAllVisits()
        visits.forEach((visit) => {
            this.renderVisit(visit)
            this.visitsList.push(visit)
        })
    }
            
    async addVisit() {
        const doctor = document.querySelector('.js-doctor-select').value
        const firstNameUnCase = document.querySelector('.js-first-name-input').value
        const firstName = firstNameUnCase.slice(0, 1).toUpperCase() + firstNameUnCase.slice(1).toLowerCase()
        const fathersNameUnCase = document.querySelector('.js-father-name-input').value
        const fathersName = fathersNameUnCase.slice(0, 1).toUpperCase() + fathersNameUnCase.slice(1).toLowerCase()
        const lastNameUnCase = document.querySelector('.js-last-name-input').value
        const lastName = lastNameUnCase.slice(0, 1).toUpperCase() + lastNameUnCase.slice(1).toLowerCase()
        const purpose = document.querySelector('.js-purpose-input').value
        const description = document.querySelector('.js-description-input').value
        const urgency = document.querySelector('.js-urgency-input').value
        const status = 'active'
        let unicValues = {}

        let age = null
        let lastVisit = null
        let pressure = null
        let bodyMassIndex = null

        let doctorTitle = null
        if (doctor === 'therapist') {
            doctorTitle = 'Терапевт'
            age = document.querySelector('.age').value
            unicValues.age = age
        } else if (doctor === 'dentist') {
            doctorTitle = 'Стоматолог'
            lastVisit = document.querySelector('.last-visit').value
            unicValues.lastVisit = lastVisit
        } else if (doctor === 'cardiologist') {
            doctorTitle = 'Кардиолог'
            age = document.querySelector('.age').value
            pressure = document.querySelector('.pressure').value
            bodyMassIndex = document.querySelector('.body-mass-index').value

            const diseases = []
            document.querySelectorAll('.diseases').forEach(disease => diseases.push(disease.value))
            unicValues = { age, pressure, bodyMassIndex, diseases }
        }

        const objectToPost = {doctor, doctorTitle, firstName, fathersName, lastName, purpose, description, urgency, status, unicValues}
        const responseObject = await server.postVisit(objectToPost)

        this.renderVisit(responseObject)
        this.visitsList.push(responseObject)
    }

    renderVisit(responseObject) {
        let visitDoctor

        if (responseObject.doctor === 'therapist') {
            visitDoctor = new VisitTherapist(responseObject)
        } else if (responseObject.doctor === 'dentist') {
            visitDoctor = new VisitDentist(responseObject)
        } else if (responseObject.doctor ===  'cardiologist') {
            visitDoctor = new VisitCardiologist(responseObject)
        }
        
        visitDoctor.createVisit()
        visitDoctor.showFullInfo()
    }

    removeVisit(id, card) {
        server.delete(id)
        this.visitsList.forEach(visit => {
            if (visit.id === id) {
                this.visitsList.splice(this.visitsList.indexOf(visit), 1)
            }
        })
        card.remove()
    }

    saveChanges(newObject, id, card) {
        this.getChanges(id, newObject)

        card.remove()
        this.renderVisit(newObject)
    }

    async checkVisitAsDone(id, card) {
        let response = await server.getCurrenVisits(id)
        
        card.classList.contains('active') ? response.status = 'done' : response.status = 'active'
        
        card.classList.remove('active', 'done')
        card.classList.toggle(response.status)

        this.getChanges(id, response)
    }

    async getChanges(id, newObject) {
        await server.putCurrentDoctor(id, newObject);

        this.visitsList.forEach(visit => {
            if (visit.id === id) {
                this.visitsList.splice(this.visitsList.indexOf(visit), 1, newObject)
            }
        })
    }

    filter () {
        const cards = document.querySelectorAll('.card')
        const filterInputsSelect = document.querySelectorAll('.filter-input.select')

        let filteredArray = this.visitsList

        filterInputsSelect.forEach(filterSelect => {
            const filterData = filterSelect.dataset.filter

            filteredArray = filteredArray.filter(visitObj => {
                return (filterSelect.value === visitObj[filterData] || filterSelect.value === 'all')
            })

        })

        cards.forEach(card => card.remove())
        filteredArray.forEach(object => this.renderVisit(object))
    }

    search() {
        const cards = document.querySelectorAll('.card')

        const filterName = document.querySelector('.filter-name').value

        this.visitsList.forEach(visitObj => {
            cards.forEach(card => {
                if (card.dataset.id == visitObj.id) {
                    card.classList.add('hidden')
                }

                document.querySelectorAll('.filter-input.select').forEach(filterInput => {
                    filterInput.value = 'all'
                })

                const visitArray = []
                this.getObjectValues(visitObj, visitArray)

                visitArray.forEach(string => {
                    if (string.includes(filterName)) {
                        if (card.dataset.id == visitObj.id) {
                            card.classList.remove('hidden')
                        }
                    }
                })
            })
        })
    }

    getObjectValues (visit, visitArray) {
        const valuesArray = Object.values(visit)
    
        valuesArray.forEach(value => {
            if (typeof value === 'string') {
                visitArray.push(value.toLocaleLowerCase())
            } else if (typeof value === 'array') {
                value.forEach(currentValue => {
                    visitArray.push(currentValue.toLocaleLowerCase())
                })
            } else if (typeof value === 'object') {
                this.getObjectValues(value, visitArray)
            } 
        });
    }
}

const visitsCollection = new VisitsCollection

const filterByName = document.querySelector('.filter-name')
const filterByStatus = document.querySelector('.filter-status')
const filterByUrgency = document.querySelector('.filter-urgency')

filterByName.addEventListener('input', visitsCollection.search.bind(visitsCollection))
filterByStatus.addEventListener('change', visitsCollection.filter.bind(visitsCollection))
filterByUrgency.addEventListener('change', visitsCollection.filter.bind(visitsCollection))