class Modal {
    constructor () {
        this.body = document.querySelector('body')
        this.modalWrapper
    }

    renderModalWrapper() {
        this.modalWrapper = document.createElement('div')
        this.modalWrapper.classList.add('modal-wrapper')
        this.body.appendChild(this.modalWrapper)
    }

    removeModal() {
        this.modalWrapper.remove()
    }
}
