class Visit {
    constructor(object) {
        this.doctor = object.doctor
        this.doctorTitle = object.doctorTitle
        this.firstName = object.firstName
        this.fathersName = object.fathersName
        this.lastName = object.lastName
        this.purpose = object.purpose
        this.description = object.description
        this.urgency = object.urgency
        this.id = object.id
        this.status = object.status
        this.unicValues = object.unicValues

        this.card = document.createElement('div')
        this.fullInfoModal
        this.closeFullVisitInfoModalButton
        this.openFullVisitInfoButton
    }

    createVisit() {
        const root = document.querySelector('.root')
        this.card.dataset.id = this.id

        this.card.classList.add('card', this.doctor, this.status)
        this.card.innerHTML = ` <h2 class="card__title ${this.doctor}__title">${this.doctorTitle}</h2>
                                <h3 class="card__subtitle ${this.doctor}__subtitle">Пациент</h3>
                                <div class="card__content-wrapper ${this.doctor}__wrapper">
                                    <h3 class="client-name ${this.doctor}__client-name">${this.lastName} ${this.firstName[0]}. ${this.fathersName[0]}.</h3>
                                    <h4 class="card__content-title">Цель визита:</h4>
                                    <span class="card__content-text">${this.purpose}</span>
                                    <div class="circles-container">
                                        <h4 class="card__content-title inline-title">Срочность визита:</h4>
                                        <div class="urgency-circle choose regular"></div>
                                        <div class="urgency-circle choose priority"></div>
                                        <div class="urgency-circle choose urgent"></div>
                                    </div>
                                </div>
                                <div class="card__buttons-wrapper">
                                    <button class="card__button card__visite-done-button">V</button>
                                    <button class="button card__button card__info-button js-open-full-visit-modal-button">i</button>
                                    <button class="card__button card__delete-visite-button">X</button>
                                </div>`
        
        const circle = this.card.querySelector(`.${this.urgency}`)
        circle.classList.add(`${this.urgency}--active`)

        root.prepend(this.card)

        const deleteBtn = this.card.querySelector('.card__delete-visite-button')
        deleteBtn.addEventListener('click', visitsCollection.removeVisit.bind(visitsCollection, this.id, this.card))

        const setDoneVisitBtn = this.card.querySelector('.card__visite-done-button')
        setDoneVisitBtn.addEventListener('click', visitsCollection.checkVisitAsDone.bind(visitsCollection, this.id, this.card))
    }

    addInfoModal() {
        let ruUrgency = null
        if (this.urgency === 'choose') {
            ruUrgency = 'не выбрана'
        } else if (this.urgency === 'regular') {
            ruUrgency = 'обычная'
        } else if (this.urgency === 'priority') {
            ruUrgency = 'приоритетная'
        } else if (this.urgency === 'urgent') {
            ruUrgency = 'срочная'
        } 

        this.fullInfoModal = document.querySelector('.full-card')
        this.fullInfoModal.dataset.infoId = this.id
        this.fullInfoModal.innerHTML = `
                                    <div class="full-card__content-wrapper full-card__${this.doctor}-border">
                                        <div class="full-card__buttons-wrapper">
                                            <button class="full-card__button js-close-full-visit-info-modal-button">Закрыть</button>
                                            <button class="full-card__button js-edit-btn full-card__button-down">Изменить</button>
                                        </div>
                                        <h2 class="full-card__title full-card__${this.doctor}-bg">${this.doctorTitle} </h2>
                                        <h3 class="full-card__subtitle">Пациент</h3>
                                        <h3 class="full-card__content-title full-card__${this.doctor}    -border-bottom">Полные данные</h3>
                                        <h3 class="full-card__client-name full-card__${this.doctor}-bg">${this.lastName} ${this.firstName} ${this.fathersName}</h3>
                                        <div class="full-card__info-container">
                                            <div class="full-card__info-wrapper js-all-info">
                                                <h4 class="full-card__info-title">Цель визита:</h4>
                                                <span class="full-card__info-text">${this.purpose}</span>
                                                <h4 class="full-card__info-title">Краткое описание визита:</h4>
                                                <span class="full-card__info-text">${this.description}</span>
                                                <h4 class="full-card__info-title">Срочность визита:</h4>
                                                <span class="full-card__info-text">${ruUrgency}</span>
                                            </div>
                                            <div class="full-card__info-wrapper js-info">
                                            </div>
                                        </div>
                                   </div>
        `

        this.closeFullVisitInfoModalButton = document.querySelector('.js-close-full-visit-info-modal-button')
        this.closeFullVisitInfoModalButton.addEventListener('click', fullVisitInfoModal.removeModal.bind(fullVisitInfoModal))
        fullVisitInfoModal.modalWrapper.addEventListener('click', (event) => {
            if(event.target.classList.contains('modal-wrapper')) {
                fullVisitInfoModal.removeModal()
            }
        })
    }

    showFullInfo() {
        this.openFullVisitInfoButton = this.card.querySelector('.js-open-full-visit-modal-button')
        this.openFullVisitInfoButton.addEventListener('click', fullVisitInfoModal.renderModalWrapper.bind(fullVisitInfoModal))
        this.openFullVisitInfoButton.addEventListener('click', this.addInfoModal.bind(this))
    }

    editVisit() {
        const nameContainer = this.fullInfoModal.querySelector('.full-card__client-name')
        nameContainer.innerHTML = `
                                    <input type="text" class="modal-input last-name js-last-name-edit-input name-edit-input" value="${this.lastName}" placeholder="Фамилия">
                                    <input type="text" class="modal-input first-name js-first-name-edit-input name-edit-input" value="${this.firstName}" placeholder="Имя">
                                    <input type="text" class="modal-input father-name js-father-name-edit-input name-edit-input" value="${this.fathersName}" placeholder="Отчество">
        `
        const allInfoContainer = this.fullInfoModal.querySelector('.js-all-info')
        allInfoContainer.innerHTML = `
                                        <h4 class="full-card__info-title">Цель визита:</h4>
                                        <input value="${this.purpose}" type="text" class="modal-input purpose js-purpose-edit-input edit-input" placeholder="Изменить цель">
                                        <h4 class="full-card__info-title">Краткое описание визита:</h4>
                                        <textarea style="resize: none;" class="modal-input description js-description-edit-input edit-input" placeholder="Изменить описание визита" rows="3">${this.description}</textarea>
                                        <select class="modal-input select js-urgency-input js-urgency-edit-input" name="select">
                                            <option class="option" value="regular">обычная</option>
                                            <option class="option" value="priority">приоритетная</option>
                                            <option class="option" value="urgent">неотложная</option>
                                        </select>
        `
    }

    takeChangedValues() {
        const lastName = document.querySelector('.js-last-name-edit-input').value
        const firstName = document.querySelector('.js-first-name-edit-input').value
        const fathersName = document.querySelector('.js-father-name-edit-input').value
        const purpose = document.querySelector('.js-purpose-edit-input').value
        const description = document.querySelector('.js-description-edit-input').value
        const urgency = document.querySelector('.js-urgency-edit-input').value

        let unicValues = {}

        let age = null
        let lastVisit = null
        let pressure = null
        let bodyMassIndex = null

        if (this.doctor === 'therapist') {
            age = document.querySelector('.js-age-edit-input').value
            unicValues.age = age
        } else if (this.doctor === 'dentist') {
            lastVisit = document.querySelector('.js-last-visit-edit-input').value
            unicValues.lastVisit = lastVisit
        } else if (this.doctor === 'cardiologist') {
            age = document.querySelector('.js-age-edit-input').value
            pressure = document.querySelector('.js-pressure-edit-input').value
            bodyMassIndex = document.querySelector('.js-body-mass-index-edit-input').value

            const diseases = []
            document.querySelectorAll('.js-diseases-edit-input').forEach(disease => diseases.push(disease.value))
            unicValues = { age, pressure, bodyMassIndex, diseases }
        }
        const newObject = {doctor: this.doctor, doctorTitle: this.doctorTitle, firstName: firstName, fathersName: fathersName, lastName: lastName, purpose, description, urgency: urgency, id: this.id, status: this.status, unicValues}

        visitsCollection.saveChanges(newObject, this.id, this.card)
    }
}