class VisitDentist extends Visit {
    addInfoModal() {
        super.addInfoModal()
        const div = this.fullInfoModal.querySelector('.js-info')
        
        div.innerHTML = `
        <h4 class="full-card__info-title">Дата последнего визита:</h4>
        <span class="full-card__info-text">${this.unicValues.lastVisit}</span>
        `
        
        const editBtn = this.fullInfoModal.querySelector('.js-edit-btn')
        editBtn.addEventListener('click', this.editVisit.bind(this))
    }
    editVisit() {
        super.editVisit()
        const unicInfoContainer = this.fullInfoModal.querySelector('.js-info')
        unicInfoContainer.innerHTML = `
                                        <h4 class="full-card__info-title">Дата последнего визита:</h4>
                                        <input value="${this.unicValues.lastVisit}" class="modal-input last-visit js-last-visit-edit-input edit-input" placeholder="Изменить дату">
                                        <div class="full-card__save-btn-wrapper">
                                            <button class="full-card__button js-save-btn full-card__button-save js-full-visit-info-toggle-button">Сохранить</button>
                                        </div>
        `

        const saveChangesBtn = document.querySelector('.js-save-btn')
        saveChangesBtn.addEventListener('click', this.takeChangedValues.bind(this))
        saveChangesBtn.addEventListener('click', fullVisitInfoModal.removeModal.bind(fullVisitInfoModal))
    }
}