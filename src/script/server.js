class Server {
  constructor() {
    this.token
  }

  async getToken() {
    const email = document.querySelector('.js-login-input')
    const password = document.querySelector('.js-password-input')

    if (localStorage.getItem('token') !== null) {
      this.token = localStorage.getItem('token')
      loginModal.changeAccess()
      await visitsCollection.getVisits()
      await showMassages()
    } else if (email.value !== '' && password.value !== '') {
      const response = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: `${email.value}`, password: `${password.value}` })
      })

      this.token = await response.text()
      localStorage.setItem('token', this.token)
      loginModal.removeModal()
      loginModal.changeAccess()
      await visitsCollection.getVisits()
      await showMassages()
    } else {
      loginModal.loginError()
    }
  }

  postVisit(objectToPost) {
    return fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`

      },
      body: JSON.stringify({
        doctor: objectToPost.doctor,
        doctorTitle: objectToPost.doctorTitle,
        firstName: objectToPost.firstName,
        fathersName: objectToPost.fathersName,
        lastName: objectToPost.lastName,
        purpose: objectToPost.purpose,
        description: objectToPost.description,
        urgency: objectToPost.urgency,
        status: objectToPost.status,
        unicValues: objectToPost.unicValues
      })
    })
      .then(response => response.json())
  }

  delete(cardId) {
    fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${this.token}`
      },
    })
  }

  putCurrentDoctor(cardId, visitCurrentDoctor) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`

      },
      body: JSON.stringify({
        doctor: visitCurrentDoctor.doctor,
        doctorTitle: visitCurrentDoctor.doctorTitle,
        firstName: visitCurrentDoctor.firstName,
        fathersName: visitCurrentDoctor.fathersName,
        lastName: visitCurrentDoctor.lastName,
        purpose: visitCurrentDoctor.purpose,
        description: visitCurrentDoctor.description,
        urgency: visitCurrentDoctor.urgency,
        unicValues: visitCurrentDoctor.unicValues,
        status: visitCurrentDoctor.status
      })
    })
      .then(response => response.json())
  }

  getAllVisits() {
    return fetch('https://ajax.test-danit.com/api/v2/cards', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      }
    })
      .then(response => response.json())
  }

  getCurrenVisits(cardId) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      }
    })
      .then(response => response.json())
  }
}

let server = new Server()
