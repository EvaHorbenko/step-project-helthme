class FullVisitInfoModal extends Modal {
    constructor () {
        super()
    }

    renderModalWrapper() {
        super.renderModalWrapper()
        this.modalWrapper.innerHTML = `<div class="modal modal-full-visit-info js-modal-variable-inner full-card"></div>`
    }

}

let fullVisitInfoModal = new FullVisitInfoModal()

